package user;

import javax.jws.soap.SOAPBinding;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        UserStorage userStorage = new UserStorage();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdu user:");
        String username = scanner.nextLine();

        System.out.println("Introdu parola:");
        String password = scanner.nextLine();

        User user = userStorage.getByUsername(username);
        if(user != null && user.getPassword().equals(password)) {
            System.out.println("LOGIN SUCCESS");
        } else {
            System.out.println("INVALID CREDENTIALS");
        }
    }
}
